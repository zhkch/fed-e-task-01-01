// 将下列代码用promise改进
// setTimeout(function(){
//   var a = 'hello '
//   setTimeout(function() {
//     var b = 'lagou '
//     setTimeout(function() {
//       var c = 'I ♥ U'
//       console.log(a + b + c)
//     }, 10)
//   }, 10)
// }, 10)

let promiseMaker = (value) => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      resolve(value)
    },10)
  })
}

Promise.all([
  promiseMaker('hello '),
  promiseMaker('lagou '),
  promiseMaker('I ♥ U'),
]).then((values) => {
  console.log(values.join(''))
})
