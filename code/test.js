const myPromise = require('./myPromise')

let a = new myPromise((resolve, reject) => {
  resolve('1')
})
a.then(value => console.log(value))