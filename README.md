1. 由于js是单线程的，在js执行环境中负责执行代码的线程只有一个，当需要执行耗时间的任务时，会造成阻塞，因此将任务的执行模式分为同步跟异步两种。异步任务交给幕后线程执行，当执行完毕后，会将异步任务的回调放到消息队列里面。当主线程执行完当前的所有任务后，把消息队列的第一个任务的回调拿出来执行，执行完毕继续取任务，如此循环就是一个eventLoop.

2. 根据任务粒度，异步任务分为宏任务，微任务。微任务会在当前宏任务结束之前执行，解决了实时性的问题。

   

作业：

任务1：[task1.js](<https://gitee.com/zhkch/fed-e-task-01-01/blob/master/code/task1.js>)

任务2：[task2.js](<https://gitee.com/zhkch/fed-e-task-01-01/blob/master/code/task2.js>)

任务3：[app.js](<https://gitee.com/zhkch/fed-e-task-01-01/blob/master/code/app.js>)

任务4： [myPromise.js](<https://gitee.com/zhkch/fed-e-task-01-01/blob/master/code/myPromise.js>)

